<?php

namespace App\Repository;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use App\Entity\Admin;

class AdminRepository implements UserProviderInterface
{
   
    private $connection;

   
    public function __construct()
    {
        try {

            $this->connection = new \PDO(
                "mysql:host=" . getenv("MYSQL_HOST") . ":3306;dbname=" . getenv("MYSQL_DATABASE"),
                getenv("MYSQL_USER"),
                getenv("MYSQL_PASSWORD")
            );

            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {

            dump($e);

        }
    }
    /**
     * @param string $query la requête SQL que l'on souhaite executer dans la base de données.
     * @param array|null $params les paramètres à affecter à notre requête ex: `[":id" => 1, ":name" => "toto"]`.
     * @return array|User|null 
     */
    private function fetch(string $query, array $params = []){
        try {

            $query = $this->connection->prepare($query);
          
            foreach ($params as $param => $value) {
                $query->bindValue($param, $value);
            }
   
            $query->execute();

            $result = [];
          
            foreach ($query->fetchAll() as $row) {
                $result[] = Admin::fromSQL($row);
            }
         
            if (count($result) <= 1) {
                return $result[0];
            }
           
            return $result;

        } catch (\PDOException $e) {
            dump($e);
        }

    }
    /**
     * @param Admin $user L'utilisateur à insérer dans la base de données.
     * @return Admin L'utilisateur fraîchement créé.
     */
    public function add(Admin $admin)
    {
        
        $this->fetch("INSERT INTO Admin (name, surname, email, birthdate, password) VALUES (:name, :surname, :email, :birthdate, :password)",[
            ":name" => $admin->name,
            ":surname" => $admin->surname,
            ":email" => $admin->email,
            ":birthdate" => $admin->birthdate->format("Y-m-d H:i:s"),
            ":password" => $admin->password
        ]);
      
        $admin->id = intval($this->connection->lastInsertId());
        
        return $admin;
    }

    /**
     * @param int $id L'id de l'utilisateur à selectionner.
     * @return User|null L'utilisateur correspondant à l'id.
     */
    public function get(int $id) {
        
        return $this->fetch("SELECT * FROM Admin WHERE :id", [":id" => $id]);
    }

    /**
     * @param int $id L'id de l'utilisateur à selectionner.
     * @return array|User|null L'utilisateur correspondant à l'id.
     */
    public function getAll(){
        
        return $this->fetch("SELECT * FROM user");
    }


    public function loadUserByUsername($username)
    {
        $admin = $this->fetch("SELECT * FROM Admin WHERE email=:email", [
            ":email" => $username
        ]);
        if($admin instanceof Admin) {
            return $admin;
        }
        throw new UsernameNotFoundException("User doesn't exist");
    }

    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $admin)
    {
        return $this->loadUserByUsername($admin->getUsername());
    }

    public function supportsClass($class)
    {
        return Admin::class === $class;
    }
}
