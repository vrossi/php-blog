<?php

namespace App\Repository;

use App\Entity\Article;
use App\Utils\Connect;


class ArticleRepository{

  public function getAll() : array {
$articles = [];
    try {

      $cnx = Connect::getConnection();

        $query = $cnx->prepare("SELECT * FROM Articles");
        $query->execute();

        foreach ($query->fetchAll() as $row) {
         $article = new Article();
         $article->fromSQL($row);
         $articles[] = $article;
        }

    } catch (\PDOException $e) {
      dump($e);
    }
  
    return $articles;
  }



  public function getById(int $id) {
    try {
  
      $cnx = Connect::getConnection();
  
      $query = $cnx->prepare(" SELECT * FROM Articles  WHERE id=:id");
      $query->bindValue(":id", $id);
      
      $query->execute();
  
      $result = $query->fetchAll();
      if (count($result)=== 1) {
        $article = new Article();
        $article->fromSQL($result[0]);
        return $article;
      }
  
    } catch (\PDOException $e) {
      dump($e);
    }
    return null;
  }
  
  

public function add(Article $article){
  try {

    $cnx = Connect::getConnection();

    $query = $cnx->prepare("INSERT INTO Articles (title, content, image, date) VALUES (:title, :content, :image, :date)");
    $query->bindValue(":title", $article->title);
    $query->bindValue(":content", $article->content);
    $query->bindValue(":image", $article->image);
    $query->bindValue(":date", $article->date->format('Y-m-d'));
    
  
    $query->execute();
    $article->id = intval($cnx->lastInsertId());

  } catch (\PDOException $e) {
    dump($e);
  }
}


public function delete(int $id){      
  try {
    $cnx = Connect::getConnection();

  $query = $cnx->prepare('DELETE FROM Articles WHERE id=:id');
  $query->bindValue(':id',$id);
  
  $query->execute();
  

}catch(\Exception $e) {
  dump($e);
}
return null;
}

public function update(Article $article){
  try {

    $cnx = Connect::getConnection();

    $query = $cnx->prepare(" UPDATE Articles SET title=:title, content=:content, image=:image, date=:date WHERE id=:id");
    $query->bindValue(":title", $article->title);
    $query->bindValue(":content", $article->content);
    $query->bindValue(":image", $article->image);
    $query->bindValue(":date", $article->date->format('Y-m-d'));
    $query->bindValue(":id", $article->id);
    
    
    
    $query->execute();

  } catch (\PDOException $e) {
    dump($e);
  }
  return false;

}

}
