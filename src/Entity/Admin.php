<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;


class Admin implements UserInterface
{
    public $id;
    public $name;
    public $surname;
    public $email;
    public $birthdate;
    /**
     * @Assert\Length(min=5)
     */
    public $password;

    public function __construct(string $name = null, string $surname = null, string $email = null, \DateTime $birthdate = null, string $password = null, int $id = null) {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->birthdate = $birthdate;
        $this->password = $password;
    }
   
    /** 
     * @param array 
     * @return User 
     */
    public static function fromSQL(array $rawData) {
        return new Admin(
            $rawData["name"],
            $rawData["surname"],
            $rawData["email"],
            new \DateTime($rawData["birthdate"]),
            $rawData["password"],
            $rawData["id"]
        );
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return ["ROLE_USER"];
    }

    public function getSalt()
    {
        
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        
    }


}
