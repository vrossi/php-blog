<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints\DateTime;


class Article
{

  public $id;
  public $title;
  public $content;
  public $image;
  public $date;
  public $category;

  public function fromSQL(array $sql)
  {

    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->content = $sql["content"];
    $this->image = $sql["image"];
    $this->date = \DateTime::createFromFormat("Y-m-d", $sql["date"]);
    $this->category = $sql["category"];
  }

}