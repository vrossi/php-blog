<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;


class RemoveArticleController extends Controller
{
    /**
     * @Route("/admin/remove/article/{id}", name="remove_article")
     */
    public function index(ArticleRepository $repo, Request $request,int $id) {

        $article = $repo->delete($id);

        return $this->redirectToRoute('articles');
    }
}
