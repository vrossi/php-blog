<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class UpdateArticleController extends Controller
{
    /**
     * @Route("/admin/update/{id}", name="update_article")
     */
    public function index(int $id, ArticleRepository $repo, Request $request){

        $article = $repo->getById($id);

        $form = $this->createFormBuilder($article)
        ->add('title')
        ->add('content', TextareaType::class)
        ->add('image', UrlType::class)
        ->add('date', DateType::class)
        ->add('save', SubmitType::class)
        ->getForm();
  
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            
           $repo->update($form->getData());
            return $this->redirectToRoute("articles");
        }      
        return $this->render('update_article/index.html.twig', [
           "form" => $form->createView(),
           "article" => $article
        ]);
    }

}
