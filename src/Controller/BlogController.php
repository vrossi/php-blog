<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Entity\Admin;
use App\Form\UserType;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\AdminRepository;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;




class BlogController extends AbstractController
{

  /**
   * @Route ("/inscription", name="create")
   */
  public function create(Request $request, AdminRepository $repo, UserPasswordEncoderInterface $encoder){
    $user = new Admin();
    $form = $this->createForm(UserType::class, $user);

    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()) {
        $user = $form->getData();
        $user->password = $encoder->encodePassword($user, $user->password);
        $repo->add($user);
        

        return $this->redirectToRoute('conection');
    }

        return $this->render('createacount.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
    * @Route ("/", name="home")
    * 
    */
     public function home() {

    return $this->redirectToRoute("articles");
    }
 

  /**
   * @Route ("/conection", name="conection")
   * 
   */
  public function conect(Request $request, AuthenticationUtils $authenticationUtils) {


    $error = $authenticationUtils->getLastAuthenticationError();
   

    return $this->render("conection.html.twig", []);
  }

  /**
   * @Route ("/admin/articles", name="articles")
   * @Security("has_role('ROLE_USER')")
   */
  public function show(ArticleRepository $repo) {
    $article = $repo->getAll();

    return $this->render("myarticles.html.twig", [
      'article' => $article
    ]);
  }


    /**
     * @Route("/admin/article/{id}", name="article")
     */
    public function index(int $id , ArticleRepository $repo) {
        $article = $repo->getById($id);

        return $this->render('article.html.twig', [
            "article" => $article
        ]);
    }





}